const express = require("express")
const http = require("http")
const WebSocket = require("ws")
const calc = require("./src/modules/operations")


const app = express()
const server = http.createServer(app)

const wss = new WebSocket.Server({ server })

wss.on("connection", (ws) => {  
  ws.on("message", (calcMessage) => {
    const message = JSON.parse(calcMessage)
    console.log("message received: ", message)
    console.log("message received: ", message.operator)
    
    let response = 0
    switch (message.operator) {
      case '+':
         response = calc.sum(message.number1 , message.number2) 
      break
      
      case '-':
        response = calc.subtraction(message.number1 , message.number2)
      break
      
      case '*':
        response = calc.multi(message.number1 , message.number2)
      break

      case '/':
        response = calc.div(message.number1 , message.number2)
      break
      default:
        break
    }
    console.log(response)
    ws.send(response)
  })
})

//Inicia o servidor
server.listen(process.env.PORT || 9898, () => {
  console.log("Online server on port:", server.address().port)
})
