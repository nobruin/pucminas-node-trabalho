"use strict"

const socket = new WebSocket("ws://localhost:9898/")
const socketStatus = document.getElementById("status")

const display = document.getElementById("display")
const numbers = document.querySelectorAll("[id*=key]")
const operations = document.querySelectorAll("[id*=operation]")

let newNumber = true
let operator
let previosNumber


const isPendentCalc = () => operator !== undefined
const getDisplayValue = () => display.textContent.replace(',','.')


socket.onerror = (error) => {
  console.log("WebSocket Error: ", error)
}

socket.onopen = (event) => {
  socketStatus.innerHTML = "Conectado ao servidor: " + event.currentTarget.url
  socketStatus.className = "open"
}

socket.onmessage = (event) => {
  let response = event.data.replace('.',',')
  console.log(response)
  updateDisplay(response)
}

socket.onclose = (event) => {
  socketStatus.innerHTML = "Websocket desconectado."
  socketStatus.className = "closed"
}

const sendCalc = () => {
  if (isPendentCalc()) {
    const atualNumber = getDisplayValue()

    const calcInterface = {
      number1: previosNumber,
      number2: atualNumber,
      operator: operator,
    }
    newNumber = true
    
    console.log("enviando via websocket")
    console.log(calcInterface)

    socket.send(JSON.stringify(calcInterface))
  }
}

const updateDisplay = (text) => {
  if (newNumber) {
    display.textContent = text
    newNumber = false
  }else{
    display.textContent += text
  }
}

const insertNumber = (event) => updateDisplay(event.target.textContent)

numbers.forEach((number) => number.addEventListener("click", insertNumber))

const selectOperator = (event) => {
  if (!newNumber) {
    sendCalc()
    newNumber = true
    operator = event.target.textContent
    previosNumber = getDisplayValue()
  }
}

operations.forEach((operator) => operator.addEventListener("click", selectOperator))

const startEqual = () => {
  sendCalc()
  operator = undefined
}

const cleanDisplay = () => (display.textContent = "")

const cleanCalc = () => {
  cleanDisplay()
  operator = undefined
  previosNumber = undefined
  newNumber = true
}

const backspace = () => display.textContent = display.textContent.slice(-1)
const reverseSignal = () => {
    newNumber = true
    socket.send(JSON.stringify({ 
        number1: display.textContent,
        number2: -1,
        operator: "*",}))
}

const comaExists = () => display.textContent.indexOf(',') !== -1
const valueExists = () => display.textContent.length > 0

const addComma = () => {
    if(!comaExists()){
        if(valueExists()){
            updateDisplay(',')
            return
        }

        updateDisplay('0,')
    }
}

document.getElementById("equals").addEventListener("click", startEqual)
document.getElementById("cleanDisplay").addEventListener("click", cleanDisplay)
document.getElementById("cleanCalc").addEventListener("click", cleanCalc)
document.getElementById("backspace").addEventListener("click", backspace)
document.getElementById('reverse').addEventListener('click', reverseSignal)
document.getElementById('comma').addEventListener('click', addComma)

const keyboardMap = {
    '0': 'key0',
    '1': 'key1',
    '2': 'key2',
    '3': 'key3',
    '4': 'key4',
    '5': 'key5',
    '6': 'key6',
    '7': 'key7',
    '8': 'key8',
    '9': 'key9',
    '+': 'operationSum',
    '-': 'operationSub',
    '/': 'operationDivision',
    '*': 'operationMulti',    
    'Enter': 'equals',
    'Backspace':'backspace',
    'Escape':'cleanCalc',
    'Delete':'cleanDisplay'
}

const keyToKeyboard = (event) => {
    const key = event.key
    if(Object.keys(keyboardMap).indexOf(key)===-1){
        return
    }   
    document.getElementById(keyboardMap[key]).click()
}

document.addEventListener('keydown',keyToKeyboard )